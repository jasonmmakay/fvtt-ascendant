# v0.0.2
## Color table rolling
* Set attributes on the message so that Dice So Nice! can show 3d dice rolls
* Add support for 'other' RVs
* Add a rebalanced RV table, which can be enabled via game settings
* Fix a bug where an incorrect RV could be shown for all color results (d100=96 orange)
* Add a test mode where the result of the d100 can be specified

## Basic character sheets
* Entering a delta for bar attributes on a token is no longer clamped

# v0.0.1
## Basic character sheets
* Health, determination and hero points are top-level resources, with health and determination tracked by token bars
* Primary attributes might, agility, valor, resolve, insight and charisma
* Secondary attribute initiative, defaulted to valor

## Color table rolling
* Enter an RV and display the color result
* With no RV, roll and show the RV necessary to get each color result

## Combat
* Roll initiative for each actor in combat
* Break initiative ties first by initiative attribute, then by d9999
