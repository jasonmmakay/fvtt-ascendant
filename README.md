# Ascendant System

Basic support for the Ascendant super hero role playing game, implemented on
top of the Simple Worldbuilding System.


## Actor Sheets

Actor sheets support the main character resources Health, Determination and
Hero Points; the primary statistics Might, Agility, Valor, Resolve, Insight
and Charisma; and the secondary statistic Initiative.

![Actor Sheet](doc/sheet.png)

Both token bars can accept deltas (values prefixed with plus or minus) that
result in negative or greater-than-max values for the attribute.

![Token Before Delta](doc/token-before-delta.png)
![Token Enter Delta](doc/token-enter-delta.png)
![Token After Delta](doc/token-after-delta.png)


## Combat

Initiative is rolled with double tie-breaks, first on the Initiative stat, and
then with an additional die roll.

![Initiative Rolls](doc/initiative.png)
![Combat Tracker](doc/combat.png)


## RV Rolls

Every set of controls for all players has a `meteor` button that will open a
dialog where an RV can be entered, resulting in a D100 roll and the color
result being sent to the chat. If the RV value is left blank, instead a D100
is rolled and the RV necessary to acheive each color result (other than white)
is calculated and sent to the chat.

Checking the 'Other RV' accepts RV in multiples of 0.5 for unopposed rolls.
Finally, entering 'd100=X' before an RV (or without one for all color rolls),
fixes the result of the die roll to the value X, and marks the result 'fixed'.

![RV Roll Dialog](doc/rv.png)


## System Settings

In addition to the standard Simple Worldbuilding System settings, a toggle is
provided to enable an alternate, rebalanced color table that more closely
approximates the doubling of the expectation value of the outcomes (hat tip
to @alexmooney for the number crunching).

![System Settings](doc/system.png)
