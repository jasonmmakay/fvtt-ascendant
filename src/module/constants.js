export const ATTRIBUTE_TYPES = ["String", "Number", "Boolean", "Formula", "Resource"];

export const RV_TABLE = [
    [ {min: 0, max:   0}, {min:   0, max:   0}, {min:   0, max:   0}, {min:   0, max:   0}, {min:   1, max: 100} ],   //  -9
    [ {min: 0, max:   0}, {min:   0, max:   0}, {min:   0, max:   0}, {min:   1, max:   1}, {min:   2, max: 100} ],   //  -8
    [ {min: 0, max:   0}, {min:   0, max:   0}, {min:   1, max:   1}, {min:   2, max:   3}, {min:   4, max: 100} ],   //  -7
    [ {min: 0, max:   0}, {min:   1, max:   1}, {min:   2, max:   3}, {min:   4, max:   7}, {min:   8, max: 100} ],   //  -6
    [ {min: 0, max:   0}, {min:   1, max:   2}, {min:   3, max:   6}, {min:   7, max:   9}, {min:  10, max: 100} ],   //  -5
    [ {min: 0, max:   0}, {min:   1, max:   3}, {min:   4, max:   7}, {min:   8, max:  12}, {min:  13, max: 100} ],   //  -4
    [ {min: 0, max:   0}, {min:   1, max:   4}, {min:   5, max:   9}, {min:  10, max:  19}, {min:  20, max: 100} ],   //  -3
    [ {min: 0, max:   0}, {min:   1, max:   6}, {min:   7, max:  13}, {min:  14, max:  25}, {min:  26, max: 100} ],   //  -2
    [ {min: 0, max:   0}, {min:   1, max:   7}, {min:   8, max:  19}, {min:  20, max:  35}, {min:  36, max: 100} ],   //  -1
    [ {min: 1, max:   1}, {min:   2, max:  11}, {min:  12, max:  26}, {min:  27, max:  50}, {min:  51, max: 100} ],   //   0
    [ {min: 1, max:   2}, {min:   3, max:  13}, {min:  14, max:  33}, {min:  34, max:  67}, {min:  68, max: 100} ],   //   1
    [ {min: 1, max:   3}, {min:   4, max:  20}, {min:  21, max:  50}, {min:  51, max:  98}, {min:  99, max: 100} ],   //   2
    [ {min: 1, max:   9}, {min:  10, max:  37}, {min:  38, max:  79}, {min:  80, max:  99}, {min: 100, max: 100} ],   //   3
    [ {min: 1, max:  23}, {min:  24, max:  55}, {min:  56, max:  98}, {min:  99, max:  99}, {min: 100, max: 100} ],   //   4
    [ {min: 1, max:  47}, {min:  48, max:  95}, {min:  96, max:  98}, {min:  99, max:  99}, {min: 100, max: 100} ],   //   5
    [ {min: 1, max:  96}, {min:  97, max:  97}, {min:  98, max:  98}, {min:  99, max:  99}, {min: 100, max: 100} ],   //   6
    [ {min: 1, max: 100}, {min: 999, max: 999}, {min: 999, max: 999}, {min: 999, max: 999}, {min: 999, max: 999} ] ]; //   7

export const RV_TABLE_REBALANCED = [
    [ {min: 0, max:   0}, {min:   0, max:   0}, {min:   0, max:   0}, {min:   0, max:   0}, {min:   1, max: 100} ],   //  -9
    [ {min: 0, max:   0}, {min:   0, max:   0}, {min:   0, max:   0}, {min:   1, max:   1}, {min:   2, max: 100} ],   //  -8
    [ {min: 0, max:   0}, {min:   0, max:   0}, {min:   1, max:   1}, {min:   2, max:   3}, {min:   4, max: 100} ],   //  -7
    [ {min: 0, max:   0}, {min:   1, max:   1}, {min:   2, max:   3}, {min:   4, max:   7}, {min:   8, max: 100} ],   //  -6
    [ {min: 0, max:   0}, {min:   1, max:   2}, {min:   3, max:   5}, {min:   6, max:   9}, {min:  10, max: 100} ],   //  -5
    [ {min: 0, max:   0}, {min:   1, max:   3}, {min:   4, max:   7}, {min:   8, max:  12}, {min:  13, max: 100} ],   //  -4
    [ {min: 0, max:   0}, {min:   1, max:   4}, {min:   5, max:   9}, {min:  10, max:  19}, {min:  20, max: 100} ],   //  -3
    [ {min: 0, max:   0}, {min:   1, max:   6}, {min:   7, max:  13}, {min:  14, max:  25}, {min:  26, max: 100} ],   //  -2
    [ {min: 0, max:   0}, {min:   1, max:   8}, {min:   9, max:  19}, {min:  20, max:  36}, {min:  37, max: 100} ],   //  -1
    [ {min: 1, max:   1}, {min:   2, max:  11}, {min:  12, max:  24}, {min:  25, max:  50}, {min:  51, max: 100} ],   //   0
    [ {min: 1, max:   2}, {min:   3, max:  15}, {min:  16, max:  34}, {min:  35, max:  69}, {min:  70, max: 100} ],   //   1
    [ {min: 1, max:   3}, {min:   4, max:  20}, {min:  21, max:  50}, {min:  51, max:  98}, {min:  99, max: 100} ],   //   2
    [ {min: 1, max:   8}, {min:   9, max:  37}, {min:  38, max:  78}, {min:  79, max:  99}, {min: 100, max: 100} ],   //   3
    [ {min: 1, max:  23}, {min:  24, max:  56}, {min:  57, max:  97}, {min:  98, max:  99}, {min: 100, max: 100} ],   //   4
    [ {min: 1, max:  44}, {min:  45, max:  96}, {min:  97, max:  98}, {min:  99, max:  99}, {min: 100, max: 100} ],   //   5
    [ {min: 1, max:  96}, {min:  97, max:  97}, {min:  98, max:  98}, {min:  99, max:  99}, {min: 100, max: 100} ],   //   6
    [ {min: 1, max: 100}, {min: 999, max: 999}, {min: 999, max: 999}, {min: 999, max: 999}, {min: 999, max: 999} ] ]; //   7

export const RV_COLORS = [ 'red', 'orange', 'yellow', 'green', 'white' ];
