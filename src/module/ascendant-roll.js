import { RV_TABLE } from './constants.js';
import { RV_TABLE_REBALANCED } from './constants.js';
import { RV_COLORS } from './constants.js';


// Get the RV table, original or rebalanced, depending on the game setting
function getRVTable() {
  const rebalancedRV = game.settings.get("ascendant", "rebalancedRV");
  if (rebalancedRV)
    return RV_TABLE_REBALANCED;
  else
    return RV_TABLE;
}


// Format an 'other' RV value to ensure it is a multiple of 0.5
function formatOtherRV(rv) {
  const rvs = (rv >= 0) ? '' : '-';
  const rv2 = Math.round(Math.abs(rv) * 2);
  const rvi = Math.trunc(rv2 / 2).toString();
  const rvf = (rv2 % 2 === 0) ? '.0' : '.5';
  return rvs + rvi + rvf;
}


// Roll d100 or emulate a roll with a fixed result value
async function d100(value=null) {
  let roll = new Roll('1d100');
  if (value !== null) {
    roll = new Roll(`${value}`);
  }

  await roll.evaluate();

  return roll;
}


// Roll and show the RV needed for each color result
async function allColorsRoll(isOther, fixedRoll=null) {
  const rvTable = getRVTable();
  const roll = await d100(fixedRoll);

  let results = [];
  for (let colorIdx = 0; colorIdx < 4; colorIdx++) {
    results.push('n/a');
    for (let rvIdx = 0; rvIdx < 17; rvIdx++) {
      if (rvTable[rvIdx][colorIdx].min <= roll.total
          && roll.total <= rvTable[rvIdx][colorIdx].max) {
        if (isOther)
          results[colorIdx] = formatOtherRV((rvIdx - 9) / 2);
        else
          results[colorIdx] = rvIdx - 9;
        break;
      }
    }
  }

  ChatMessage.create({
    type: CONST.CHAT_MESSAGE_STYLES.IC,
    rolls: [roll],
    content: `
      <div class="dice-roll">
        <div class="dice-result">
          <div class="dice-formula">
            ${fixedRoll === null ? '' : 'FIXED'}
            D100 = ${roll.total}
          </div>
          <div class="dice-total" style="background-color:${RV_COLORS[0]}">
            RV = ${results[0]}
          </div>
          <div class="dice-total" style="background-color:${RV_COLORS[1]}">
            RV = ${results[1]}
          </div>
          <div class="dice-total" style="background-color:${RV_COLORS[2]}">
            RV = ${results[2]}
          </div>
          <div class="dice-total" style="background-color:${RV_COLORS[3]}">
            RV = ${results[3]}
          </div>
        </div>
      </div>
      ` });
}


// Roll on the table for a specified RV
async function rvRoll(rvRaw, isOther, fixedRoll=null) {
  const rvTable = getRVTable();

  rvRaw = parseFloat(rvRaw);
  let rvLookup = 0;

  if (isOther)
  {
    rvLookup = Math.round(2 * rvRaw);
    rvRaw = formatOtherRV(rvLookup / 2);
  }
  else
  {
    rvLookup = Math.round(rvRaw);
    rvRaw = rvLookup;
  }

  const rvIdx = Math.min(Math.max(rvLookup, -9), 7) + 9;

  const roll = await d100(fixedRoll);

  let color = null;
  for (let colorIdx = 0; colorIdx < 5; colorIdx++) {
    if (rvTable[rvIdx][colorIdx].min <= roll.total
        && roll.total <= rvTable[rvIdx][colorIdx].max) {
      color = RV_COLORS[colorIdx];
      break;
    }
  }

  ChatMessage.create({
    type: CONST.CHAT_MESSAGE_STYLES.IC,
    rolls: [roll],
    content: `
      <div class="dice-roll">
        <div class="dice-result">
          <div class="dice-formula">
            RV = ${rvRaw}
          </div>
          <div class="dice-formula">
            ${fixedRoll === null ? '' : 'FIXED'}
            D100 = ${roll.total}
          </div>
          <div class="dice-total" style="background-color:${color}">
            ${color}
          </div>
        </div>
      </div>
      ` });
}


// Determine which roller function to call depending on whether the
// input is empty or not or if a test d100 value is specified
async function rvResolver() {
  const isOther = document.getElementById('ascendant-rv-other-check').checked;
  const rv = document.getElementById('ascendant-rv-input').value;

  let match = rv.match(/^\s*(?:d100=(?<roll>\d+)\s+)?(?<rv>[-+]?[0-9]*\.?[0-9]+)\s*$/i);
  if (match)
      return await rvRoll(match.groups.rv, isOther, match.groups.roll);

  match = rv.match(/^\s*(?:d100=(?<roll>\d+))?\s*$/i);
  if (match)
    return await allColorsRoll(isOther, match.groups.roll);

  ui.notifications.error('Invalid RV entry format');
}


// Display the dialog box for entering RV
export function rvDialog() {
  Dialog.prompt({
    title: game.i18n.localize('ASCENDANT.EnterRV'),
    content: `
      <input type="text" id="ascendant-rv-input">
      <input type="checkbox" id="ascendant-rv-other-check">
      <label>${game.i18n.localize('ASCENDANT.OtherRV')}</label><br>
      <label>${game.i18n.localize('ASCENDANT.BlankRV')}</label>
      `,
    label: 'Roll D100',
    callback: async () => { await rvResolver() } } );
}
