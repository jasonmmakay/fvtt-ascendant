VERSION = unset
PROJECT = ascendant
RELEASEDIR = releases/$(VERSION)

package :
ifeq ($(VERSION),unset)
	$(error VERSION must be set as command line argument with VERSION=X.Y.Z)
endif
	if [ ! -d $(RELEASEDIR) ]; then mkdir -p $(RELEASEDIR); fi
	if [ -f $(RELEASEDIR)/$(PROJECT).zip ]; then rm $(RELEASEDIR)/$(PROJECT).zip; fi
	zip -r $(RELEASEDIR)/$(PROJECT).zip src/
	cp src/system.json $(RELEASEDIR)/
	git add .
	git status

amend : package
	git commit --amend --no-edit
